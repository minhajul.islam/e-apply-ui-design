import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import  'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class EApplyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: Icon(Icons.list_outlined,color: Colors.white,),
          iconSize: 30.0,
        ),
        title: Text('E-Apply'),
        actions: [
          IconButton(icon: Icon(Icons.notification_important_sharp,color: Colors.white,), onPressed: null),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Image.asset('images/profile.png',height: 70,width: 70,),
          )
          ],
          elevation:80.0,
      ),
      body:StaggeredGridView.count(
          crossAxisCount: 6,
        crossAxisSpacing: 12.0,
        mainAxisSpacing: 12.0,
        padding: EdgeInsets.symmetric(horizontal: 16.0,vertical: 8.0),
        children: [
          //First Row Tiles ......
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24.0),
              color: Colors.black,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("001",style: TextStyle(fontSize: 18.0,color: Colors.white),),
                      SizedBox(height: 8.0,),
                      Center(
                        child: Text("Applied",style: TextStyle(
                          color: Colors.white54,
                        ),),
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("001",style: TextStyle(fontSize: 18.0,color: Colors.white),),
                      SizedBox(height: 8.0,),
                      Center(
                        child: Text("Ticket",style: TextStyle(
                          color: Colors.white54,
                        ),),
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("2500 Tk",style: TextStyle(fontSize: 18.0,color: Colors.white),),
                      SizedBox(height: 8.0,),
                      Center(
                        child: Text("Balance",style: TextStyle(
                          color: Colors.white54,
                        ),),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),

          //Second Row first tile....
          Container(
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(24.0),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("images/addmission.png",height: 50.0,width: 50.0,),
                    SizedBox(height: 8.0,),
                    Text("Admission Apply",style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                    ),
                    ),
                  ],
                )
              ],
            ),
          ),
          //second row 2nd tile......
          Container(
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(24.0),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("images/bus.png",height: 50.0,width: 50.0,),
                    SizedBox(height: 8.0,),
                    Text("Exam Bus Service",style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                    ),
                    ),
                  ],
                )
              ],
            ),
          ),
          //third row tiles.....
          Container(
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(24.0),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("images/foundation.png",height: 60.0,width: 50.0,),
                    SizedBox(height: 8.0,),
                    Text("Foundation",style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                    ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(24.0),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("images/sms.png",height: 60.0,width: 50.0,),
                    SizedBox(height: 8.0,),
                    Text("SMS Alert",style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                    ),
                    ),
                  ],
                )
              ],
            ),
          ),

          Container(
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(24.0),
            ),
            child: Center(
              child: Wrap(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset("images/suggestion.png",height: 50.0,width: 50.0,),
                      SizedBox(height: 8.0,),
                      Text("Executive Suggestion",textAlign: TextAlign.center ,style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w300,
                      ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
        staggeredTiles: [
          StaggeredTile.extent(6, 150.0,),
          StaggeredTile.extent(3, 150.0,),
          StaggeredTile.extent(3, 150.0,),
          StaggeredTile.extent(2, 170.0),
          StaggeredTile.extent(2, 170.0),
          StaggeredTile.extent(2, 170.0),


        ],
      )
    );
  }
}
